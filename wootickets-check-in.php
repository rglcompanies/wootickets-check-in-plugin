<?php
/**
 * Plugin Name: WooTickets Check In
 * Description: Manages events' attendance
 * Version: 1.0.1
 * Author: Henrikas Elsbergas
 * License: GPL2
 */

/*
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if (!defined('ABSPATH')) exit; // Exit if accessed directly

if (!class_exists('WooticketsCheckIn')) {

    class WooticketsCheckIn
    {
        const TICKET_KEY_LENGTH = 10;

        /**
         * Init plugin
         */
        public function __construct()
        {
            // set unique ticket numbers for each ticket
            add_action( 'woocommerce_checkout_order_processed', array($this, 'woocommerce_order_processed'));

            // add admin menu entry
            add_action('admin_menu', array($this, 'admin_menu'));

            // add ajax action for ticket fetching
            add_action('wp_ajax_wootickets_get_tickets', array($this, 'wp_ajax_wootickets_get_tickets'));

            // add check in ajax action
            add_action('wp_ajax_wootickets_check_in', array($this, 'wp_ajax_wootickets_check_in'));

            // add check out ajax action
            add_action('wp_ajax_wootickets_check_out', array($this, 'wp_ajax_wootickets_check_out'));

            // add check in progress ajax action
            add_action('wp_ajax_wootickets_check_in_progress', array($this, 'wp_ajax_wootickets_check_in_progress'));

            // add ajax action returning events for android app
            add_action('wp_ajax_nopriv_wootickets_app_get_events', array($this, 'wp_ajax_nopriv_wootickets_app_get_events'));

            // add ajax action for ticket check in
            add_action('wp_ajax_nopriv_wootickets_app_check_in', array($this, 'wp_ajax_nopriv_wootickets_app_check_in'));

            // add ajax action for manual ticket check in
            add_action('wp_ajax_nopriv_wootickets_app_manual_enter_code', array($this, 'wp_ajax_nopriv_wootickets_app_manual_enter_code'));

            // add admin init action
            add_action('admin_init', array($this, 'add_attendee_csv_export'));
        }

        /**
         * Woocommerce callback fired after order is created
         *
         * @param $order_id
         */
        public function woocommerce_order_processed($order_id)
        {
            $this->ensure_unique_ticket_keys($order_id);
        }

        /**
         * Create unique keys for each ticket and save them in order_itemmeta if not yet present
         *
         * @param $order_id
         */
        public function ensure_unique_ticket_keys($order_id)
        {
            $order = new WC_Order($order_id);
            $items = $order->get_items();

            // assign unique ticket keys if not yet assigned
            foreach ($items as $id => $item) {
                $current_status_meta = woocommerce_get_order_item_meta($id, '_ticket_statuses', true);
                $current_statuses = json_decode($current_status_meta, true);

                if (empty($current_statuses)) {
                    $quantity = $item['qty'];
                    $ticket_statuses = array();

                    for ($i = 0; $i < $quantity; $i++)
                        // ticket not checked in by default
                        $ticket_statuses[$this->get_unique_ticket_key()] = false;

                    // make sure there is no entry before adding
                    woocommerce_delete_order_item_meta($id, '_ticket_statuses');
                    woocommerce_add_order_item_meta($id, '_ticket_statuses', json_encode($ticket_statuses), true);
                }
            }
        }

        /**
         * Return unique key for a ticket
         *
         * @return string
         */
        private function get_unique_ticket_key()
        {
            // does not contain zero and O for better visibility
            $characters = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

            do {
                $key = '';

                for ($i = 0; $i < self::TICKET_KEY_LENGTH; $i++)
                    $key .= $characters[mt_rand(0, strlen($characters) - 1)];
            } while (! $this->is_unique_ticket_key($key));

            return $key;
        }

        /**
         * Check uniqueness of ticket key
         *
         * @param $key
         * @return bool
         */
        private function is_unique_ticket_key($key) {
            global $wpdb;
            $results = $wpdb->get_results( 'SELECT meta_value FROM ' . $wpdb->prefix . 'woocommerce_order_itemmeta WHERE meta_key = "_ticket_statuses"', ARRAY_A );

            foreach ($results as $row) {
                if (strstr($row['meta_value'], $key)) {
                    return false;
                }
            }

            return true;
        }

        /**
         * Android app check in action
         * pass ticket_key to $_POST['data'] and event_id to $_POST['event_id']
         */
        public function wp_ajax_nopriv_wootickets_app_check_in()
        {
            $ticket_key = $_POST['data'];
            $event_id = $_POST['event_id'];

            $order_item_id = $this->get_order_item_id_by_ticket_key($ticket_key);
            $order_id = $this->get_order_id_by_order_item_id($order_item_id);

            $response = array(
                'status' => false,
                'message' => 'Invalid ticket',
                'fullname' => 'N/A',
                'zone' => 'N/A',
                'seat' => 'N/A'
            );

            if ($order_item_id && $order_id) {
                $product_id = woocommerce_get_order_item_meta($order_item_id, '_product_id', true);
                $status = get_the_terms($order_id, 'shop_order_status');
                $status = empty($status) ? null : $status[0];

                if ($status && $status->slug == 'completed') {
                    if ($this->check_event_match($product_id, $event_id)) {

                        // check that ticket has not been checked in yet
                        if ($this->get_ticket_status($ticket_key) == false) {
                            // add one to event's checked in ticket quantity
                            $this->update_event_check_in_quantity($event_id, 1);

                            // check in ticket
                            $this->update_ticket_status($ticket_key, true);

                            $response['status'] = true;
                            $response['message'] = 'Successfully checked in';
                        } else {
                            $response['message'] = 'Already checked in';
                        }

                        $response['fullname'] = $this->get_name_from_order($order_id);
                        $response['zone'] = $this->get_ticket_zone($product_id);
                        $response['seat'] = $this->get_ticket_seat($product_id);
                    } else {
                        $response['message'] = 'Ticket is for another event';
                    }
                }
            }

            echo json_encode($response);
            die;
        }

        /**
         * Action for android app manual check in first step
         */
        public function wp_ajax_nopriv_wootickets_app_manual_enter_code()
        {
            $ticket_key = $this->get_complete_ticket_key(strtoupper($_POST['data']));
            $event_id = $_POST['event_id'];

            $response = array(
                'found' => false,
                'status' => false,
                'message' => 'Invalid code',
                'fullname' => 'N/A',
                'zone' => 'N/A',
                'seat' => 'N/A',
                'ticket_key' => $ticket_key
            );

            if ($ticket_key) {
                $response['found'] = true;

                $order_item_id = $this->get_order_item_id_by_ticket_key($ticket_key);
                $order_id = $this->get_order_id_by_order_item_id($order_item_id);

                if ($order_item_id && $order_id) {
                    $product_id = woocommerce_get_order_item_meta($order_item_id, '_product_id', true);
                    $status = get_the_terms($order_id, 'shop_order_status');
                    $status = empty($status) ? null : $status[0];

                    if ($status && $status->slug == 'completed') {
                        if ($this->check_event_match($product_id, $event_id)) {

                            // check that ticket has not been checked in yet
                            if ($this->get_ticket_status($ticket_key) == false) {
                                $response['status'] = true;
                                $response['message'] = 'Confirm to check in';
                                $response['zone'] = $this->get_ticket_zone($product_id);
                                $response['seat'] = $this->get_ticket_seat($product_id);
                            } else {
                                $response['message'] = 'Already checked in';
                            }

                            $response['fullname'] = $this->get_name_from_order($order_id);
                            $response['zone'] = $this->get_ticket_zone($product_id);
                            $response['seat'] = $this->get_ticket_seat($product_id);
                        } else {
                            $response['message'] = 'Ticket is for another event';
                        }
                    }
                }
            }

            echo json_encode($response);
            die;
        }

        /**
         * Returns a ticket key if it can be be uniquely identified from first part of the key
         *
         * @param $partial_key string at least 5 chars long
         * @return null|string
         */
        private function get_complete_ticket_key($partial_key) {
            // minimum key length is 5 characters
            if (strlen($partial_key) > 4) {
                global $wpdb;
                $results = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_itemmeta
                    WHERE meta_key = "_ticket_statuses" AND meta_value LIKE "%' . $partial_key . '%"', ARRAY_A);

                // if unique key identified
                if (count($results) == 1)
                    foreach ($results as $row) {
                        $ticketStatuses = json_decode($row['meta_value'], true);
                        foreach ($ticketStatuses as $key => $status) {
                            // return if code starts with partial_key
                            if (strpos($key, $partial_key) === 0)
                                return $key;
                        }
                    }
            }

            return null;
        }

        /**
         * Returns true if ticket is checked in or cannot be found
         * false if ticket was found and has not been checked in yet
         *
         * @param $ticket_key
         * @return bool
         */
        private function get_ticket_status($ticket_key) {
            global $wpdb;
            $results = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_itemmeta
                WHERE meta_key = "_ticket_statuses" AND meta_value LIKE "%' . $ticket_key . '%"', ARRAY_A);

            foreach ($results as $row) {
                $ticketStatuses = json_decode($row['meta_value'], true);


                return isset($ticketStatuses[$ticket_key]) ? $ticketStatuses[$ticket_key] : true;
            }

            return true;
        }

        /**
         * Returns order_item_id for given ticket_key or null if not found
         *
         * @param $ticket_key
         * @return null|int
         */
        private function get_order_item_id_by_ticket_key($ticket_key)
        {
            global $wpdb;
            $results = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_itemmeta
                WHERE meta_key = "_ticket_statuses" AND meta_value LIKE "%' . $ticket_key . '%"', ARRAY_A);

            foreach ($results as $row) {
                return $row['order_item_id'];
            }

            return null;
        }

        /**
         * Returns order id for given order_item_id or null if not found
         *
         * @param $order_item_id
         * @return null
         */
        private function get_order_id_by_order_item_id($order_item_id) {
            global $wpdb;
            $results = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_items
                WHERE order_item_id = ' . $order_item_id, ARRAY_A);

            foreach ($results as $row) {
                return $row['order_id'];
            }

            return null;
        }

        /**
         * Returns true if given product_id is used for given event, false otherwise
         *
         * @param $product_id
         * @param $event_id
         * @return bool
         */
        private function check_event_match($product_id, $event_id) {
            $category_id = get_post_meta($event_id, 'product_from_category', true);
            $event_products = $this->get_products_in_category($category_id);

            return in_array($product_id, $event_products);
        }

        /**
         * Ajax action checking connection between app and site instance
         */
        public function wp_ajax_nopriv_wootickets_app_get_events()
        {
            echo json_encode($this->get_events());
            die;
        }

        /**
         * Ajax action taking event_id as a parameter and returning ticketCount, checkedInTicketCount and
         * progress percentage
         */
        public function wp_ajax_wootickets_check_in_progress()
        {
            // get event id from POST
            $event_id = $_POST['event_id'];

            // get ticket quantities attached to event
            $ticketCount = get_post_meta($event_id, 'wootickets_event_ticket_qty', true);
            $checkedInTicketCount = get_post_meta($event_id, 'wootickets_event_checked_in_ticket_qty', true);

            // check if ticket counts were set, return 0 if not
            $ticketCount = empty($ticketCount) ? 0 : $ticketCount;
            $checkedInTicketCount = empty($checkedInTicketCount) ? 0 : $checkedInTicketCount;

            // avoid division by zero, return zero
            if ($ticketCount > 0)
                $progress = $checkedInTicketCount / $ticketCount * 100;
            else
                $progress = 0;

            // render response
            wp_send_json_success(compact('ticketCount', 'checkedInTicketCount', 'progress'));
        }

        /**
         * Ajax check in action
         */
        public function wp_ajax_wootickets_check_in()
        {
            $ticket_key = $_POST['ticket_key'];
            $event_id = $_POST['event_id'];

            // add one to event's checked in ticket quantity
            $this->update_event_check_in_quantity($event_id, 1);

            // check in ticket
            $this->update_ticket_status($ticket_key, true);

            wp_send_json_success();
        }

        /**
         * Ajax check out action
         */
        public function wp_ajax_wootickets_check_out()
        {
            $ticket_key = $_POST['ticket_key'];
            $event_id = $_POST['event_id'];

            // subtract one to event's checked in ticket quantity
            $this->update_event_check_in_quantity($event_id, -1);

            // check out ticket
            $this->update_ticket_status($ticket_key, false);


            wp_send_json_success();
        }

        /**
         * Updates ticket status
         *
         * @param $ticket_key string
         * @param $status boolean
         */
        private function update_ticket_status($ticket_key, $status)
        {
            global $wpdb;
            $results = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'woocommerce_order_itemmeta
                WHERE meta_key = "_ticket_statuses" AND meta_value LIKE "%' . $ticket_key . '%"', ARRAY_A );

            foreach ($results as $row) {
                $tickets = json_decode($row['meta_value'], true);

                if (isset($tickets[$ticket_key])) {
                    $tickets[$ticket_key] = $status;
                }

                woocommerce_update_order_item_meta($row['order_item_id'], '_ticket_statuses', json_encode($tickets));
            }
        }

        /**
         * Returns a list of people.
         * If checked in is true returns only people who have checked in,
         * if false returns people who haven't checked in
         * returns all if null or not specified
         *
         * @param $event_id
         * @internal param null $checkedInAttendees
         * @internal param bool $checkedIn
         * @return array
         */
        public function get_event_tickets($event_id) {
            $tickets = array();

            $category_id = get_post_meta($event_id, 'product_from_category', true);

            // product ids for this event
            $product_ids = array();

            // add products ids from category
            if ($category_id)
                $product_ids = $this->get_products_in_category($category_id);

            // calculate how many tickets were sold and how many of them were checked in
            $even_ticket_qty = 0;
            $event_checked_in_ticket_qty = 0;

            $orders = $this->get_orders_query($event_id);

            if ($orders->have_posts()) {
                while ($orders->have_posts()) {
                    $orders->the_post();

                    $order = new WC_Order(get_the_ID());
                    $items = $order->get_items();

                    foreach ($items as $item_id => $item) {
                        if (isset($item['product_id'])) {
                            if (in_array($item['product_id'], $product_ids)) {
                                // get how many tickets were checked in
                                $checkedInQty = 0;

                                $this->ensure_unique_ticket_keys($order->id);
                                $ticketStatuses = json_decode(woocommerce_get_order_item_meta($item_id, '_ticket_statuses', true), true);

                                foreach ($ticketStatuses as $key => $checkedIn) {
                                    // set seat name
                                    $seat = $this->get_ticket_seat($item['product_id']);

                                    // add to checked in quantity if checked in
                                    $checkedIn ? $checkedInQty++ : null;

                                    // add ticket to the list
                                    $tickets[] = array(
                                        'key' => $key,
                                        'checkin_status' => $checkedIn,
                                        'order_id' => $order->id,
                                        'product_id' => $item['product_id'],

                                        'fullname' => $this->get_name_from_order($order->id),
                                        'email' => $this->get_email_from_order($order->id),
                                        'seat' => $seat,
                                        'zone' => $this->get_ticket_zone($item['product_id']),
                                    );
                                }
                                // update statistics
                                $even_ticket_qty += $item['qty'];
                                $event_checked_in_ticket_qty += $checkedInQty;
                            } else {
                                continue;
                            }
                        } else {
                            // trying to find a matching product for this order item according to it's name
                            global $wpdb;
                            $results = $wpdb->get_results( 'SELECT ID FROM ' . $wpdb->prefix . 'posts WHERE post_type = "product" AND post_title = "' . $item['name'] . '"', ARRAY_A);

                            if (! empty($results)) {
                                wc_add_order_item_meta($item_id, '_product_id', $results[0]['ID']);
                            }
                        }
                    }
                }
            }
            wp_reset_query();

            // update event statistics
            update_post_meta($event_id, 'wootickets_event_ticket_qty', $even_ticket_qty);
            update_post_meta($event_id, 'wootickets_event_checked_in_ticket_qty', $event_checked_in_ticket_qty);

            return $tickets;
        }

        /**
         * Returns a WP_Query object holding all the orders for the specified event
         *
         * @param $event_id
         * @return WP_Query
         */
        private function get_orders_query($event_id) {
            $args = array(
                'post_type' => 'shop_order',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'shop_order_status',
                        'field' => 'slug',
                        'terms' => array('completed')
                    )
                ),
                // NOPE, this did not work out...
                // only fetch orders created after event creation date
//                'date_query' => array(
//                    'after' => get_the_time('Y-m-d H:i:s', $event_id)
//                )
            );

            return new WP_Query($args);
        }

        /**
         * Ajax action returning a list of tickets for the specified event
         */
        public function wp_ajax_wootickets_get_tickets()
        {
            $event_id = $_GET['event_id'];
            $tickets = $this->get_event_tickets($event_id);

            foreach ($tickets as $i => $ticket) {
                $tickets[$i] = array(
                    $this->get_product_check_in_buttons($ticket['key'], $ticket['checkin_status']),
                    '#' . $ticket['order_id'],
                    $ticket['key'],
                    $ticket['fullname'],
                    $ticket['seat'],
                    $ticket['zone']
                );
            }

            // render response
            echo json_encode(array('data' => $tickets));
            die;
        }

        /**
         * Returns seat name for the given product
         *
         * @param $product_id int
         * @return string
         */
        private function get_ticket_seat($product_id) {
            $product = new WC_Product($product_id);
            return $product->get_title();
        }

        /**
         * Returns fullname of a customer
         *
         * @param $order_id
         * @return string
         */
        private function get_name_from_order($order_id) {
            $firstName = get_post_meta($order_id, '_billing_first_name', true);
            $lastName = get_post_meta($order_id, '_billing_last_name', true);

            return $firstName . ' ' . $lastName;
        }

        private function get_email_from_order($order_id) {
            return get_post_meta($order_id, '_billing_email', true);
        }

        /**
         * Returns a string containing ticket zones
         *
         * @param $product_id
         * @return string
         */
        private function get_ticket_zone($product_id)
        {
            $categories = wp_get_post_terms($product_id, 'product_cat');
            $zone = '';

            foreach ($categories as $i => $category) {
                $zone .= $category->name;

                if ($i < count($categories) - 1)
                    $zone .= ', ';
            }

            return $zone;
        }

        /**
         * Adds javascript files to check in panel
         *
         * @param $hook
         */
        public function enqueue_check_in_js($hook)
        {
            if ('toplevel_page_wootickets-check-in' != $hook)
                return;

            $js = array(
                'wootickets.checkin.js.bootstrap.datatables' => 'public/plugins/datatables/js/jquery.dataTables.js',
                'wootickets.checkin.js.application' => 'public/js/application.js'
            );

            foreach ($js as $key => $file)
                wp_enqueue_script($key, plugin_dir_url(__FILE__) . $file);

            // pass config to js
            wp_localize_script('wootickets.checkin.js.application', 'WooticketsConfig', array(
                'admin_url' => get_admin_url()
            ));

        }

        /**
         * Adds stylesheets to check in panel
         *
         * @param $hook
         */
        public function enqueue_check_in_css($hook)
        {
            if ('toplevel_page_wootickets-check-in' != $hook)
                return;

            $css = array(
                'wootickets.checkin.css.bootstrap.datatables' => 'public/plugins/datatables/css/jquery.dataTables.css',
                'wootickets.checkin.css.datatables' => 'public/css/application.css'
            );

            foreach ($css as $key => $file)
                wp_enqueue_style($key, plugin_dir_url(__FILE__) . $file);
        }

        /**
         * Includes necessary js and css and adds link to "Ticket Check in" in admin menu
         */
        public function admin_menu()
        {
            add_action('admin_enqueue_scripts', array($this, 'enqueue_check_in_js'));
            add_action('admin_enqueue_scripts', array($this, 'enqueue_check_in_css'));

            add_menu_page('Ticket Check In', 'Ticket Check In', 'manage_options', 'wootickets-check-in',
                array($this, 'check_in_panel'), 'dashicons-list-view', 56);
        }

        /**
         * Echoes html for cheeck in admin page
         */
        public function check_in_panel()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            echo '
<div class="wrap">
    <h2>Ticket Check In</h2>
    <div id="ticket-check-in-notifications"></div>
    <div class="bootstrap-wpadmin">
        <div class="row">
            <div class="col-xs-3">
                <select id="app-event-select">' . $this->get_event_options() . '</select>
            </div>

            <div class="col-xs-5">
                <div id="app-check-in-progress" class="progress">
                    <div id="app-progress-percents"></div>
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0"
                         aria-valuemin="0" aria-valuemax="100" style="width: 0">
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div id="app-progress-info" style="text-align: center">
                    <span id="app-checked-in-count">0</span> out of
                    <span id="app-total-ticket-count">0 </span> checked in
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <input id="app-ticket-search" type="text" class="form-control" placeholder="Search for Atendees by Ticket number, Name, Seat number or Ticket Type"/>
                <table id="app-tickets-table" class="display dataTable" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Order ID</th>
                        <th>Ticket number</th>
                        <th>Name</th>
                        <th>Seat Number</th>
                        <th>Zone</th>
                    </tr>
                    </thead>

                    <tfoot>
                    <tr>
                        <th>Status</th>
                        <th>Order ID</th>
                        <th>Ticket number</th>
                        <th>Name</th>                        
                        <th>Seat Number</th>
                        <th>Zone</th>
                    </tr>
                    </tfoot>

                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>';
        }

        public function add_attendee_csv_export() {
            if (isset($_GET['action']) && $_GET['action'] == "attendee_export" && isset($_GET['event_id'])) {
                $event_id = $_GET['event_id'];
                $tickets = $this->get_event_tickets($event_id);
                $f = fopen('php://memory', 'w');
                // loop over the input array
                foreach ($tickets as $ticket) {
                    $record = array(
                        $ticket['checkin_status'] ? '1' : '0',
                        $ticket['key'],
                        $ticket['fullname'],
                        $ticket['email']

                    );
                    // generate csv lines from the inner arrays
                    fputcsv($f, $record, ",");
                }

                $events = $this->get_events();
                $filename = $events[$event_id] . '.csv';

                // rewrind the "file" with the csv lines
                fseek($f, 0);
                // tell the browser it's going to be a csv file
                header('Content-Type: application/csv');
                // tell the browser we want to save it instead of displaying it
                header('Content-Disposition: attachement; filename="' . $filename . '";');
                // make php send the generated csv lines to the browser
                fpassthru($f);
                exit;
            }
        }

        /**
         * Adds diff to specified event checked in ticket quantity
         *
         * @param $event_id
         * @param $diff
         */
        private function update_event_check_in_quantity($event_id, $diff)
        {
            $ticketQty = get_post_meta($event_id, 'wootickets_event_checked_in_ticket_qty', true);
            update_post_meta($event_id, 'wootickets_event_checked_in_ticket_qty', $ticketQty + $diff);
        }

        /**
         * Returns and array of product ids in specified category
         *
         * @param $category_id
         * @return array
         */
        private function get_products_in_category($category_id)
        {
            $products = array();
            $args = array(
                'post_type' => 'product',
                'post_status' => array('publish', 'private'),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'terms' => $category_id
                    )
                )
            );

            $my_query = null;
            $my_query = new WP_Query($args);

            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    $products[] = get_the_ID();
                }
            }

            wp_reset_query();
            return $products;
        }


        /**
         * Returns check in and check out buttons for specified order item
         * Hides one of the buttons depending on checkedIn parameter
         *
         * @param $ticket_key
         * @param bool $checkedIn
         * @return string
         */
        private function get_product_check_in_buttons($ticket_key, $checkedIn = false)
        {
            $undoButtonClass = '';
            $checkInButtonClass = '';

            if ($checkedIn)
                $checkInButtonClass = 'hide';
            else
                $undoButtonClass = 'hide';

            return '<button data-ticket-key="' . $ticket_key . '" type="button"
                            class="' . $undoButtonClass . ' app-check-out btn btn-default">
                        Undo Check In
                    </button>
                    <button data-ticket-key="' . $ticket_key . '" type="button"
                            class="' . $checkInButtonClass . ' app-check-in btn btn-success">
                        Check In
                    </button>';
        }

        /**
         * Returns html option string for all upcoming events
         * suitable for select inputs
         *
         * @return string
         */
        private function get_event_options()
        {
            $event_options = '';
            $events = $this->get_events();

            foreach ($events as $id => $title) {
                $event_options .= '<option value="' . $id . '">' . $title . '</option>';
            }

            return $event_options;
        }

        /**
         * Returns a list of upcoming/happening events in id => title format
         *
         * @return array
         */
        private function get_events()
        {
            $events = array();
            $args = array(
                'post_type' => 'incsub_event',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'incsub_event_end',
                'order' => 'ASC',
                'ignore_sticky_posts' => 1,

//                uncomment to show only upcomming events
//
//                'meta_key' => 'incsub_event_end',
//                'meta_value' => date('Y-m-d H:i:s'),
//                'meta_compare' => '>='
            );

            $my_query = null;
            $my_query = new WP_Query($args);

            if ($my_query->have_posts()) {
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    $events[get_the_ID()] = get_the_title();
                }
            }

            wp_reset_query();

            return $events;
        }
    }

    function wootickets_check_in_init()
    {
        $GLOBALS['wootickets_check_in'] = new WooticketsCheckIn();
    }

    add_action('init', 'wootickets_check_in_init');
}
