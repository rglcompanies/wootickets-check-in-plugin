TedXEvents = {
    Config: {
        baseUrl: ''
    }
};

jQuery(document).ready(function ($) {
    // init table
    var table = jQuery('#app-tickets-table').DataTable({
        "dom": '<"tickets-table-top">rt<"tickets-table-bottom"li><"clearfix"><"tickets-table-pagination"p><"clearfix">'
    });

    jQuery('#app-ticket-search').on('keyup', function () {
        table
            .search('"' + this.value + '"')
            .draw();
    });

    var pagination = jQuery('#app-tickets-table_paginate');
    var width = pagination.width();

    pagination.css({
        width: width + 100,
        float: 'none'
    });


    var reloadProgress = function () {
        var eventId = jQuery('#app-event-select').val();

        if (!isNaN(parseInt(eventId))) {
            jQuery.post(ajaxurl, { action: 'wootickets_check_in_progress', event_id: eventId})
                .done(function (response) {
                    var percentage = response['data']['progress'],
                        ticketCount = response['data']['ticketCount'],
                        checkedInCount = response['data']['checkedInTicketCount'];

                    jQuery('#app-check-in-progress').find('.progress-bar')
                        .attr('aria-valuenow', percentage)
                        .css('width', percentage + '%');
                    jQuery('#app-progress-percents').html(Math.floor(percentage) + '%');

                    jQuery('#app-checked-in-count').html(checkedInCount);
                    jQuery('#app-total-ticket-count').html(ticketCount);
                });
        }
    };

    var loadEventTickets = function (event_id) {
        table.ajax.url(ajaxurl + '?action=wootickets_get_tickets&event_id=' + event_id).load();
    };

    // init event select
    jQuery('#app-event-select').change(function () {
        var event_id = jQuery(this).val();

        loadEventTickets(event_id);
        reloadProgress();

        $('#app-tickets-table_length .app-attendees-export').hide().remove();
        $('#app-tickets-table_length').prepend('<a href="'
            + WooticketsConfig.admin_url + "?action=attendee_export&event_id=" + event_id
            + '" target="_blank" class="app-attendees-export btn btn-success">Export CSV</a>');
    }).change();

    // check in button click
    jQuery('.app-check-in').live('click', function () {
        var ticketKey = jQuery(this).data('ticket-key'),
            container = jQuery(this).closest('tr'),
            eventId = jQuery('#app-event-select').val();

        jQuery.post(ajaxurl, { action: 'wootickets_check_in', ticket_key: ticketKey, event_id: eventId})
            .done(function () {
                reloadProgress();

                jQuery('.app-check-in', container).hide();
                jQuery('.app-check-out', container).removeClass('hide').show();

                // add one from checked in count
                jQuery('#app-checked-in-count').each(function () {
                    jQuery(this).html(parseInt(jQuery(this).html()) + 1);
                });
            });
    });

    // check out button click
    jQuery('.app-check-out').live('click', function () {
        var ticketId = jQuery(this).data('ticket-key'),
            container = jQuery(this).closest('tr'),
            eventId = jQuery('#app-event-select').val();

        jQuery.post(ajaxurl, { action: 'wootickets_check_out', ticket_key: ticketId, event_id: eventId})
            .done(function () {
                reloadProgress();

                jQuery('.app-check-out', container).hide();
                jQuery('.app-check-in', container).removeClass('hide').show();

                // subtract one from checked in count
                jQuery('#app-checked-in-count').each(function () {
                    jQuery(this).html(parseInt(jQuery(this).html()) - 1);
                });
            });
    });

    setInterval(function () {
        var event_id = jQuery('#app-event-select').val();
        loadEventTickets(event_id);
        reloadProgress();
    }, 30 * 1000);

    // check internet connection every minute
    setInterval(function () {
        $.get(document.URL, {}).done(function () {
            $("#ticket-check-in-notifications .connection-error").hide().remove();
        }).fail(function () {
            if ($("#ticket-check-in-notifications .connection-error").size() == 0) {
                $("#ticket-check-in-notifications").append(
                        '<div class="connection-error error">' +
                        '<p>Connection error. Please check your cable or wifi signal</p>' +
                        '</div>'
                );
            }
        })
    }, 10 * 1000)
});
